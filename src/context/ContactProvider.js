import React, { useState, useEffect } from 'react';
import {
    collection,
    addDoc,
    getDocs,
    doc,
    deleteDoc,
    updateDoc,
    setDoc,
    getDoc,
    query,
    where,
} from 'firebase/firestore';
import { db } from '../FirebaseConfig';

export const ContactContext = React.createContext();

const ConstactsProvider = (props) => {
    const [contacts, setContacts] = useState([]);

    const onContactSubmit = async (contact) => {
        console.log("Chegou na função");
        try {
            const docRef = await addDoc(collection(db, 'faleconosco'), contact);
            console.log('Document written with ID: ', docRef.id);
            contact.id = docRef.id;
            const newContacts = [...contacts, contact];
            setContacts(newContacts);
        } catch (error) {
            console.log('Error adding document: ', error);
        }
    };

    return (
        <ContactContext.Provider value={{
            contacts: contacts,
            onContactSubmit: onContactSubmit
        }}>
            {props.children}
        </ContactContext.Provider>
    );
};

export default ConstactsProvider;
