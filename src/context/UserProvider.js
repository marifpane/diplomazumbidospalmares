import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import {
    collection,
    addDoc,
    getDocs,
    doc,
    deleteDoc,
    updateDoc,
    setDoc,
    getDoc,
    query,
    where,
} from 'firebase/firestore';
import { db, auth } from '../FirebaseConfig';
import { createUserWithEmailAndPassword } from "firebase/auth";
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";



export const UsersContext = React.createContext();

const UsersProvider = (props) => {
    const [uid, setFuid] = useState();
    const [Research, setResearch] = useState(false);
    const [users, setUsers] = useState([]);
    const [logado, setLogado] = useState(false);
    const navigate = useNavigate()

    const onUserSubmit = async (user, id) => {
        try {
            const docRef = await setDoc(doc(db, 'Users',id), {
                id: id,
                name: user.name,
                birthdate: user.birthdate,
                email: user.email,
                favorites: []
            });
            const newUser = [...users, user];
            setUsers(newUser);
        }
        catch (error) {
            console.log('Error adding document: ', error);
        }
    };

    const createUSer = async (user) => {
        console.log("Passou funcao");
        const userCredential = createUserWithEmailAndPassword(auth, user.email, user.password, user)
            .then((userCredential) => {
                setLogado(true);
                const userVa = userCredential.user;
                onUserSubmit(user, userVa.uid);
                navigate("/");
                // ...
            })
            .catch((error) => {
                const errorCode = error.code;
                const errorMessage = error.message;
                // ..
            });
    };

    const autenticar = async (email, password) => {

        signInWithEmailAndPassword(auth, email, password)
            .then((userCredential) => {
                // Signed in
                const user = userCredential.user;
                setLogado(true);
                setFuid(user.uid);
                console.log("Autenticado");
                navigate("/");
                // ...
            })
            .catch((error) => {
                const errorCode = error.code;
                const errorMessage = error.message;
            });
    }


    return (
        <UsersContext.Provider value={{
            users: users,
            onUserSubmit: onUserSubmit,
            createUSer: createUSer,
            logado: logado,
            autenticar: autenticar,
            setLogado: setLogado,
            Research: Research,
            setResearch: setResearch,
            uid:uid
            
            

        }}>
            {props.children}
        </UsersContext.Provider>
    );
};

export default UsersProvider;
