import React, { useState, useEffect } from 'react';
import {
  collection,
  addDoc,
  getDocs,
  doc,
  deleteDoc,
  updateDoc,
  setDoc,
  getDoc,
  query,
  where,
} from 'firebase/firestore';
import { db } from '../FirebaseConfig';

export const HomenageadosContext = React.createContext();

const HomenageadosProvider = (props) => {
  const [homenageados, setHomenageados] = useState([]);

  useEffect(() => {
    fetchHomanageados();
  }, []);

  const fetchHomanageados = async () => {
    const homenageadosAux = [];
    try {
      const querySnapshot = await getDocs(collection(db, 'homenageados'));
      querySnapshot.forEach((doc) => {
        console.log(`${doc.id} => ${doc.data()}`);
        homenageadosAux.push({ id: doc.id, ...doc.data() });
      });
      setHomenageados(homenageadosAux);
    } catch (error) {
      console.error('Error fetching collection: ', error);
    }
  };

  const onHomenageadoSubmit = async (homenageado) => {
    try {
      const docRef = await addDoc(collection(db, 'homenageados'), homenageado);
      console.log('Document written with ID: ', docRef.id);
      homenageado.id = docRef.id;
      const newHomenageados = [...homenageados, homenageado];
      setHomenageados(newHomenageados);
    } catch (error) {
      console.log('Error adding document: ', error);
    }
  };

  return (
    <HomenageadosContext.Provider value={{
      homenageados: homenageados,
      onHomenageadoSubmit: onHomenageadoSubmit,
      fetchHomanageados: fetchHomanageados
    }}>
      {props.children}
    </HomenageadosContext.Provider>
  );
};

export default HomenageadosProvider;
