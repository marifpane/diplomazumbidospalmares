import React, { useContext, useState } from 'react';
import './Card.css';
import { UsersContext } from "../../context/UserProvider";
import { updateDoc, arrayUnion, arrayRemove, doc } from 'firebase/firestore';
import { db } from '../../FirebaseConfig';

const Card = (props) => {
  const userCtx = useContext(UsersContext);
  const [favorited, setFavorited] = useState(false);
  const uid = userCtx.uid;
  const toggleFavorite = async () => {
    console.log("aqui",props.homenageado.id);
    console.log(uid);
    const userRef = doc(db, 'Users', uid); // Supondo que você tenha o ID do usuário disponível no contexto

    console.log("mariana", userRef);
    try {
      if (!favorited) {
        // Adicionar o ID do homenageado ao array "favorites"
        await updateDoc(userRef, {
          favorites: arrayUnion(props.homenageado.id)
        });
      } else {
        // Remover o ID do homenageado do array "favorites"
        await updateDoc(userRef, {
          favorites: arrayRemove(props.homenageado.id)
        });
      }
      setFavorited(!favorited);
    } catch (error) {
      console.error('Erro ao favoritar:', error);
    }
  };

  return (
    <div className={`card ${favorited ? 'favorited' : ''}`}>
      <img src={props.homenageado.imagem} alt="Foto da Pessoa" className="card-image" />
      <div className="card-content">
        <div className="card-info">
          <h2>{props.homenageado.nome}</h2>
          <p>Data de Nascimento: {props.homenageado.dataDeNascimento}</p>
          <p>Data de Falecimento: {props.homenageado.dataDeFalecimento}</p>
        </div>
        <p className="card-biography">
          Biografia: {props.homenageado.biografia}
        </p>
        {userCtx.logado &&
          <button className={`favorite-button ${favorited ? 'favorited' : ''}`} onClick={toggleFavorite}>

            <span className="favorite-icon">&#9733;</span>
            <span className="favorite-text">Favoritar</span>
          </button>}
      </div>
    </div>
  );
};

export default Card;

