import React, { useContext, useState } from 'react';
import "../App.css";
import { NavLink, useNavigate } from 'react-router-dom';
import Homenageados from "./Homenageados";
import SearchBar from './SearchBar/SearchBar';
import { UsersContext } from "../context/UserProvider";


export default function App() {
  const userCtx = useContext(UsersContext);
  const uid = userCtx.uid;

  console.log(uid);

  const admin =  () => {
    if (String(uid) === "UxQVS7ApF7ZAFy0gRNEidEBFvKh1") {
      return true;
    }
    return false;

  }

  console.log(admin());


  const navigate = useNavigate()
  return (
    <div>
      <h1>Premiados</h1>
      <div>
        <div className="card-container">
          <div className=".button-container-add" style={{ display: "flex", alignItems: "center", justifyContent: "center", margin: "2rem 0 2rem 0" }}>
            {admin() && userCtx.logado ? <button onClick={() => navigate("/premiados/CadastrarReg")} className="add-button" >Adicionar mais Premiados</button> : 
            <></>}
          </div>
        </div>
      </div>
      <SearchBar />
    </div>
  )
}