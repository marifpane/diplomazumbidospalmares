import React, { useContext, useEffect,useState } from 'react';
import { HomenageadosContext } from '../context/HomenageadosProvider';
import Card from '../Componentes/Card/Card'

const Homenageados = () => {

  const [homenageadosdata, setHomenageadosData] = useState([{
    nome: "",
    dataDeNascimento: "",
    dataDeFalecimento: "",
    biografia: "",
    imagem: ""
  }]);
  
  const { homenageados } = useContext(HomenageadosContext);

  useEffect(()=>{
    setHomenageadosData(homenageados);
  }, [homenageados])

  return (
    <div>
      {homenageadosdata.map((homenageado) => {
        return (
          <Card key={homenageado.id} homenageado={homenageado}></Card>
        );
      })}
    </div>
  );
};
export default Homenageados;