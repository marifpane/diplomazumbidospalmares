import React, { useContext, useState } from 'react';
import './ContactForm.css';
import { ContactContext, onContactSubmit } from "../../context/ContactProvider";

function ContactForm() {
  const ContactCtx = useContext(ContactContext);
  const [contact, setContacts] = useState({
    nome: "",
    email: "",
    assunto: "",
    mensagem: ""
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(contact);
    ContactCtx.onContactSubmit(contact);
    setContacts({
      nome: "",
      email: "",
      assunto: "",
      mensagem: ""
    })
  };

  return (
    <div>
      <main>
        <h1 id="fale">Fale Conosco</h1>
        <div className="contact-form-container">
          <form onSubmit={handleSubmit}>
            <p>
              <label htmlFor="nome">Nome:</label>
            </p>
            <input
              type="text"
              id="nome"
              value={contact.nome}
              onChange={(e) => setContacts({ ...contact, nome: e.target.value })}
              placeholder="Digite seu nome..."
              name="nome" required />
            <p>
              <label htmlFor="email">Email:</label>
            </p>
            <input
              type="email"
              id="email"
              value={contact.email}
              onChange={(e) => setContacts({ ...contact, email: e.target.value })}
              placeholder="Digite seu email..."
              name="email" required />

            <p>
              <label htmlFor="assunto">Assunto:</label>
            </p>
            <input
              type="text"
              id="assunto"
              value={contact.assunto}
              onChange={(e) => setContacts({ ...contact, assunto: e.target.value })}
              name="assunto"
              placeholder="Escreva o motivo do contato..." required />

            <p>
              <label htmlFor="mensagem">Mensagem:</label>
            </p>
            <textarea
              id="mensagem"
              name="mensagem"
              value={contact.mensagem}
              onChange={(e) => setContacts({ ...contact, mensagem: e.target.value })}
              placeholder="Descreva em uma breve mensagem sua necessidade..."
              rows="5"
              required
            ></textarea>

            <p>
              <input type="submit" value="Enviar" />
            </p>
          </form>
        </div>
      </main>
      <hr />
    </div>
  );
}

export default ContactForm;
