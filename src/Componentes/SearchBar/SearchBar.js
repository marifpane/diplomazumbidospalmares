import React, { useContext, useState, useEffect } from 'react';
import { db } from '../../FirebaseConfig';
import {
    collection,
    addDoc,
    getDocs,
    doc,
    deleteDoc,
    updateDoc,
    setDoc,
    getDoc,
    query,
    where,
} from 'firebase/firestore';
import './SearchBar.css';
import Card from '../Card/Card';
import { UsersContext } from "../../context/UserProvider";
import {HomenageadosContext} from "../../context/HomenageadosProvider";

function SearchBar() {
    const userCtx = useContext(UsersContext);
    const {fetchHomanageados,homenageados} = useContext(HomenageadosContext);
    const [searchTerm, setSearchTerm] = useState('');
    const [searchResults, setSearchResults] = useState(homenageados);

    //useEffect(() => {
      //  handleSearch();
    //}, []);

    const handleSearch = async () => {
        const homenageadosFilterAux = [];
        if (searchTerm === "") {
            //fetchHomanageados();
            setSearchResults(homenageados);
            return;
        }
        try {
            const querySnapshot = await getDocs(collection(db, 'homenageados'));
            //const usersRef = collection(db, 'homenageados');
            //const q = query(usersRef, where("nome", searchTerm));
            //const querySnapshot = await getDocs(q);
            console.log("Aqui a query", querySnapshot.docs.filter(item => item.data().nome.toLowerCase().includes(searchTerm.toLowerCase())));
            querySnapshot.docs.filter(item => item.data().nome.toLowerCase().includes(searchTerm.toLowerCase())).forEach((doc) => {
                console.log(doc.id, " >= ", doc.data());
                homenageadosFilterAux.push({ id: doc.id, ...doc.data() });
            });
            console.log("homenageados: ", homenageadosFilterAux);
            //const results = querySnapshot.docs.map(doc => doc.data());
            setSearchResults(homenageadosFilterAux);
        } catch (error) {
            console.error('Error searching users:', error);
        }
    };

    return (
        <div >
            <div style={{ display: "flex", alignItems: "center", justifyContent: "center", margin: "2rem 0 2rem 0" }}>
                <div className="search-bar-container">
                    <input
                        className="search-input"
                        type="text"
                        value={searchTerm}
                        onChange={e => setSearchTerm(e.target.value)}
                        placeholder="Digite um nome para buscar"
                    />
                    <div>
                        <button className="search-button" onClick={handleSearch}>Buscar</button>
                    </div>
                </div>
            </div>

            {searchResults.length > 0 && (
                <div>
                    {searchResults.map((homenageado) => {
                        return (
                            <Card key={homenageado.id} homenageado={homenageado}></Card>
                        );
                    })}
                </div>
            )}
        </div>
    );
}

export default SearchBar;
