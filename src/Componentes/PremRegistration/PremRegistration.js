import React, { useContext, useState } from 'react';
import './PremRegistration.css';
import { NavLink, useNavigate } from 'react-router-dom';
import { HomenageadosContext } from "../../context/HomenageadosProvider"

const CadastroPremiado = () => {
  const HomenageadoCtx = useContext(HomenageadosContext);
  const [homenageado, setHomenageados] = useState({
    nome: "",
    dataDeNascimento: "",
    dataDeFalecimento: "",
    biografia: "",
    imagem: ""
  });


  const navigate = useNavigate()

  const handleSubmit = (e) => {
    e.preventDefault();
    HomenageadoCtx.onHomenageadoSubmit(homenageado);
    setHomenageados({
      nome: "",
      dataDeNascimento: "",
      dataDeFalecimento: "",
      biografia: "",
      imagem: ""
    })
  };

  return (
    <div style={{ display: "flex", alignItems: "center", justifyContent: "center", margin: "2rem 0 2rem 0" }}>
      <div className="premiado-cadastrado-container" >
        <h1>Cadastro de Novo Premiado</h1>
        <form onSubmit={handleSubmit}>
          <div>
            <label>Nome:</label>
            <input type="text"
              value={homenageado.nome}
              onChange={(e) => setHomenageados({ ...homenageado, nome: e.target.value })} />
          </div>
          <div>
            <label>Data de Nascimento:</label>
            <input type="date"
              value={homenageado.dataDeNascimento}
              onChange={(e) => setHomenageados({ ...homenageado, dataDeNascimento: e.target.value })} />
          </div>
          <div>
            <label>Data de Falecimento:</label>
            <input type="date"
              value={homenageado.dataDeFalecimento}
              onChange={(e) => setHomenageados({ ...homenageado, dataDeFalecimento: e.target.value })} />
          </div>
          <div>
            <label>Biografia:</label>
            <textarea
              value={homenageado.biografia}
              onChange={(e) => setHomenageados({ ...homenageado, biografia: e.target.value })} />
          </div>
          <div>
            <label>Imagem:</label>
            <input
              type="text"
              value={homenageado.imagem}
              onChange={(e) => setHomenageados({ ...homenageado, imagem: e.target.value })} />
          </div>
          <div className="button-container">
            <button onClick={() => navigate("/premiados")} >Voltar</button>
            <button type="submit">Cadastrar</button>
          </div>

        </form>
      </div>
    </div>
  );
};

export default CadastroPremiado;
