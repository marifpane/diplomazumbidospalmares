import React, { useContext, useState } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import './UserRegistration.css'
import {UsersContext} from "../../context/UserProvider";

const UserRegistration = () => {
  const [error, setError] = useState('');
  const userCtx = useContext(UsersContext);
  const [user, setUsers] = useState({
    name: "",
    birthdate: "",
    email: "",
    password: "",
    confirmPassword: ""
  });

  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(user.email, user.name);
    // Validar campos e processar o envio do formulário
    if (!user.name || !user.birthdate|| !user.email|| !user.password|| !user.confirmPassword) {
      setError('Por favor, preencha todos os campos.');
    } else if (user.password  !== user.confirmPassword) {
      setError('As senhas não coincidem.');
    } else if (!user.password .match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)) {
      setError('A senha deve conter pelo menos uma letra maiúscula, uma letra minúscula, um caractere especial e um número.');
    } else {
      setError('');

      userCtx.createUSer(user);
      setUsers({
        name: "",
        birthdate: "",
        email: "",
        password: "",
        confirmPassword: ""
      })
      // Processar envio do formulário
      console.log('Formulário enviado!');
    }
  };


  return (
    <div className="container">
      <form onSubmit={handleSubmit}>
        <h2>Cadastro de Usuário</h2>
        <div>
          <label>Nome:</label>
          <input type="text" 
          value={user.name} 
          onChange={(e) => setUsers({...user, name: e.target.value})} />
        </div>
        <div>
          <label>Data de Nascimento:</label>
          <input type="date" 
          value={user.birthdate} 
          onChange={(e) => setUsers({...user, birthdate: e.target.value})} />
        </div>
        <div>
          <label>Email:</label>
          <input type="email" 
          value={user.email} 
          onChange={(e) => setUsers({...user, email: e.target.value})} />
        </div>
        <div>
          <label>Senha:</label>
          <input type="password" 
          value={user.password} 
          onChange={(e) => setUsers({...user, password: e.target.value})} />
        </div>
        <div>
          <label>Confirmar Senha:</label>
          <input type="password" 
          value={user.confirmPassword} 
          onChange={(e) => setUsers({...user, confirmPassword: e.target.value})} />
        </div>
        {error && <div className="error">{error}</div>}
        <div className="container-btn-sub">
          <button onClick={() => navigate("/login")} >Voltar</button>
          <button type="submit">Cadastrar</button>
        </div>
      </form>
    </div>
  );
};

export default UserRegistration;
