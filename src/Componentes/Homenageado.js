import React from 'react';
import Imagem from './Imagem/Imagem';

const Homenageado = (props) => {
  return (
    <>
      <p>Homenageado</p>
      <Imagem imageUrl={props.homenageado.image} />
      <p>{props.homenageado.nome}</p>
      <p>{props.homenageado.dataDeNascimento}</p>
      <p>{props.homenageado.dataDeFalecimento}</p>
      <p>{props.homenageado.biografia}</p>
    </>
  );
};
export default Homenageado;
