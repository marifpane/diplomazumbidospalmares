import React, { useContext, useState } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import './FormLogin.css';
import {UsersContext} from "../../context/UserProvider";
import { sendPasswordResetEmail } from 'firebase/auth';
import { auth } from '../../FirebaseConfig';

function redefinirSenha(){
  var email = prompt("Digite o email cadastrado: " );
  console.log(email);
  sendPasswordResetEmail(auth,email)                
  .then(() => {
      console.log('Email envado com sucesso!!');
      // Password reset email sent!
      // ..
  })
  .catch((error) => {
      var errorCode = error.code;
      var errorMessage = error.message;
      console.log('Problema ao enviar email!', error);
      // ..
  });

}


function LoginForm() {
  const [error, setError] = useState('');
  const userCtx = useContext(UsersContext);
  const [user, setUsers] = useState({
    email: "",
    password: ""
  });

  const navigate = useNavigate()

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(user.email);
    userCtx.autenticar(user.email,user.password);
  };
  return (
    <main className="login-form-container">
      <h1>Login</h1>
      <form form onSubmit={handleSubmit}>
        <label htmlFor="email">Email:</label>
        <input type="email" 
        id="email" 
        value={user.email} 
        onChange={(e) => setUsers({...user, email: e.target.value})}
        placeholder="Digite seu email" />
        <label htmlFor="password">Senha:</label>
        <input type="password" 
        value={user.password} 
        id="password" 
        onChange={(e) => setUsers({...user, password: e.target.value})}
        placeholder="Digite sua senha" />
        {error && <div className="error">{error}</div>}
        <button type="button" id="forgot-password-button" onClick={() => redefinirSenha()}>Esqueceu sua senha?</button>
        <button type="submit">Entrar</button>
        <button onClick={() => navigate("/login/Cadastrar")} >Cadastrar</button>
      </form>

    </main >
  );
}

export default LoginForm;