import React from 'react';
import './Imagem.css';

const Imagem = (props) => {
  console.log(props);
  return(
    <div className='imagemContainer'>
      <img className='imagem' src={props.imageUrl} alt=''/>
    </div>
  )
}

export default Imagem;