import React, { useContext, useState, useEffect } from 'react';
import { db } from '../FirebaseConfig';
import {
  collection,
  addDoc,
  getDocs,
  doc,
  deleteDoc,
  updateDoc,
  setDoc,
  getDoc,
  query,
  where,
} from 'firebase/firestore';
import Card from '../Componentes/Card/Card';
import { UsersContext } from "../context/UserProvider";


function Favoritos() {
  const userCtx = useContext(UsersContext);
  const [favorites, setFavorites] = useState([]);
  const [Imprime, setImprime] = useState([]);
  const uid = userCtx.uid;

  useEffect( () => {
    printFavorites();
  },[]);

  console.log("Favoritos", favorites)
  const coletaFavoritos = async () => {
    let favoritesAux = [];
    try {
      const homenageadosSnapshot = await getDocs(collection(db, 'homenageados'));
      favorites.forEach((id) => {
        homenageadosSnapshot.forEach((doc) => {
          if (id === doc.id) {
            console.log("entrou na função");
            favoritesAux.push(doc.data());
          }
        });
      });
      setImprime(favoritesAux);

    }
    catch (error) {
      console.error('Error fetching collection: ', error);
    }

  }
  useEffect(()=>
  {
    coletaFavoritos();
  },[favorites])

  const printFavorites = async () => {

    try {
      const usersRef = collection(db, 'Users');
      const q = query(usersRef, where("id", "==", String(uid)));
      const querySnapshot = await getDocs(q);
      querySnapshot.forEach((doc) => {
        console.log(doc.id, " => ", doc.data());
        setFavorites(doc.data().favorites);
      });
      console.log(favorites);

    } catch (error) {
      console.error('Error searching users:', error);
    }
  };

  return (
    <div>
      <h1>Favoritos</h1>
      <div>
        <div className="card-container">
          {Imprime.map((homenageado) => {
            return (
              <Card key={homenageado.id} homenageado={homenageado}></Card>
            );
          })}
        </div>
      </div>
    </div>
  );
}

export default Favoritos;