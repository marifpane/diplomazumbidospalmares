import React, { useContext, useState } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import './Head.css';
import { UsersContext } from "../../context/UserProvider";

const HeaderPage = () => {
  const userCtx = useContext(UsersContext);
  const navigate = useNavigate();
  console.log(userCtx.logado);

  const deslogar = async() =>{
    userCtx.setLogado(false);
    navigate("/");
  };
  

  return (
    <header style={{ fontFamily: "Arial" }}>
      <div></div>
      <div>
        <h1 style={{ margin: "1rem 0 0 0" }} >Diploma Zumbi dos Palmares</h1>
        <nav>
          <ul>
            <li>
              <NavLink to="/" className="menu-button">
                Inicio
              </NavLink>
            </li>
            <li>
              <NavLink to="/premiados " className="menu-button">
                Premiados
              </NavLink>
            </li>
            <li>
              {userCtx.logado && <NavLink to="/favoritos " className="menu-button">
                Favoritos
              </NavLink>}
            </li>
            <li>
              {!userCtx.logado && <NavLink to="/login " className="menu-button">
                Login
              </NavLink>}
              {userCtx.logado && <button onClick={() => deslogar()} className="menu-button">Sair</button>} 
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
};

export default HeaderPage;
