import React, { useState } from "react";
import { BrowserRouter } from "react-router-dom";
import HeaderPage from "./Head/Head";
import Foot from "./Foot/Foot";
import SysRoutes from "../SysRoutes";
import "../App.css";
import HomenageadosContext from "../context/HomenageadosProvider";
import UsersProvider from "../context/UserProvider";
import ConstactsProvider from "../context/ContactProvider";

function App() {
  return (
    <div>
      <BrowserRouter>
        <UsersProvider>
          <HeaderPage />
          <HomenageadosContext>
            <ConstactsProvider>
              <SysRoutes />
            </ConstactsProvider>
          </HomenageadosContext>
        </UsersProvider>
        <Foot />
      </BrowserRouter>
    </div>
  );
}

export default App;
