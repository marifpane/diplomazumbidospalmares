import React from 'react';
import UserRegistration from './UserRegistration/UserRegistration';


function Cadastrar() {
  return (
    <div style={{ display: "flex", alignItems: "center", justifyContent: "center", margin: "2rem 0 2rem 0" }}>
      <UserRegistration/>
    </div>

  );
}

export default Cadastrar;
