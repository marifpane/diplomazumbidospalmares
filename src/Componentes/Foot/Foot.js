import React from 'react';
import { NavLink } from 'react-router-dom';
import '../../App.css';
import './foot.css';

function Foot() {
  return (
    <footer className="footer-container">
      <div>
        <NavLink to="/faleconosco" className="fale-conosco-button">Fale conosco</NavLink>
        <p className="developer-text">Desenvolvido por Mariana Fragali e Pedro Vitor</p>
      </div>
    </footer>
  );
}

export default Foot;