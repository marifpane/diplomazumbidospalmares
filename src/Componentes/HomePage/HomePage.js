import React from 'react';
import './HomePage.css';

const HomePage = () => {
  return (
    <main className="home-page">
      <div className="winners-section">
      <h2>Ganhadores do Prêmio</h2>
      <div className="winner-photos">
        <a href="https://ww2.corumba.ms.gov.br/wp-content/uploads/2021/11/Foto-Rene-Marcio-CarneiroZumbi-dos-Palmares-e-Premiacao-Beleza-Negra-49-1-1024x706.jpg" target="_blank" rel="noopener noreferrer">
          <img src="https://ww2.corumba.ms.gov.br/wp-content/uploads/2021/11/Foto-Rene-Marcio-CarneiroZumbi-dos-Palmares-e-Premiacao-Beleza-Negra-49-1-1024x706.jpg" alt="Winner 1" style={{ maxWidth: '300px' }} />
        </a>
        <a href="https://www.portal.contagem.mg.gov.br/fotos/e8b757b9b56e611ea119d74a1772178a.jpg" target="_blank" rel="noopener noreferrer">
          <img src="https://www.portal.contagem.mg.gov.br/fotos/e8b757b9b56e611ea119d74a1772178a.jpg" alt="Winner 2" style={{ maxWidth: '300px' }} />
        </a>
        <a href="https://www.bauru.sp.leg.br/media/original_images/IMG_0691_77e3tpE.jpg" target="_blank" rel="noopener noreferrer">
          <img src="https://www.bauru.sp.leg.br/media/original_images/IMG_0691_77e3tpE.jpg" alt="Winner 3" style={{ maxWidth: '300px' }} />
        </a>
      </div>
      </div>
      <div className="about-section">
        <h2>Sobre o Prêmio Zumbi de Palmares</h2>
        <p>
          O Prêmio Zumbi de Palmares é uma honraria concedida anualmente para pessoas que se destacaram na luta pela
          igualdade racial e promoção da cultura afro-brasileira. Criado em homenagem a Zumbi dos Palmares, líder do
          Quilombo dos Palmares, o prêmio busca reconhecer e valorizar aqueles que contribuíram para a construção de uma
          sociedade mais justa e igualitária.
        </p>
        <p>
          Desde a sua criação, o Prêmio Zumbi de Palmares tem premiado ativistas, artistas, pesquisadores, educadores e
          outros profissionais que se dedicam a combater o racismo, preservar a cultura afro-brasileira e promover a
          inclusão social. Os ganhadores são selecionados por uma comissão composta por representantes de diferentes
          áreas e instituições comprometidos com a causa.
        </p>
      </div>
      <div className="zumbi-section">
        <h2>Quem foi Zumbi dos Palmares</h2>
        <p>
          Zumbi dos Palmares foi um líder quilombola brasileiro e símbolo da resistência contra a escravidão durante o
          período colonial. Nascido livre em 1655, Zumbi foi capturado e escravizado quando criança, sendo posteriormente
          levado para o Quilombo dos Palmares, uma comunidade autônoma formada por escravos fugitivos no estado de
          Alagoas, no Brasil.
        </p>
        <p>
          Zumbi tornou-se líder do quilombo após a morte de Ganga Zumba e liderou a resistência contra os constantes
          ataques das forças coloniais portuguesas. Seu combate pela liberdade e dignidade dos negros na época da
          escravidão é amplamente reconhecido, e sua figura se tornou um símbolo da resistência negra e da luta pela
          igualdade racial no Brasil.
        </p>
      </div>
    </main>
  );
}

export default HomePage;
