import firebase from 'firebase/compat/app';
import 'firebase/compat/storage';
import { initializeApp } from "firebase/app";
import { getStorage } from 'firebase/storage';
import { getFirestore } from "firebase/firestore";
import {getAuth} from "firebase/auth"

const firebaseConfig = {
  apiKey: "AIzaSyAsnKkscdC26JMSY6uSrZJQuOMg98f36jM",
  authDomain: "diplomazumbidospalmares-abdc9.firebaseapp.com",
  projectId: "diplomazumbidospalmares-abdc9",
  storageBucket: "diplomazumbidospalmares-abdc9.appspot.com",
  messagingSenderId: "917254323028",
  appId: "1:917254323028:web:a29598ad5a98c3b1b766c1"
};


if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const app = initializeApp(firebaseConfig);
const storage = getStorage(app);
const db = getFirestore(app);
const auth = getAuth();

export { app, db, storage, auth};
