import React from "react";
import {Routes, Route} from 'react-router-dom';
import Inicio from "./Componentes/Inicio";
import FaleConosco from "./Componentes/FaleConosco";
import Login from "./Componentes/Login";
import Premiados from "./Componentes/Premiados";
import Favoritos from "./Componentes/Favoritos";
import Cadastrar from "./Componentes/Cadastrar";
import CadastroPremiado from "./Componentes/PremRegistration/PremRegistration";

const SysRoutes = (props) => {
    return(
        
            <Routes>
                <Route path="/" element={<Inicio/>}/>
                <Route path="/faleconosco" element={<FaleConosco/>}/>
                <Route path="/favoritos" element={<Favoritos/>}/>
                <Route path="/premiados" element={<Premiados/>}/>
                <Route path="/premiados/CadastrarReg" element={<CadastroPremiado/>}/>
                <Route path="/login" element={<Login/>}/>
                <Route path="/login/Cadastrar" element={<Cadastrar/>}/>  
            </Routes>       
    )
}

export default SysRoutes;